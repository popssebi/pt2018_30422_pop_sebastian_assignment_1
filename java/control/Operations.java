package control;
import view.Interface;

import javax.swing.*;

public class Operations {

    private String finalResult ="";

    public String getFinalResult() {
        return finalResult;
    }

    public void setFinalResult(String finalResult) {
        this.finalResult = finalResult;
    }

    private int[] resultedPolynomial;

    public int[] getResultedPolynomial() {
        return resultedPolynomial;
    }

    public void setResultedPolynomial(int[] resultedPolynomial) {
        this.resultedPolynomial = resultedPolynomial;
    }

    private int sizeOfResultedPolynomial;

    public int getSizeOfResultedPolynomial() {
        return sizeOfResultedPolynomial;
    }

    public void setSizeOfResultedPolynomial(int sizeOfResultedPolynomial) {
        this.sizeOfResultedPolynomial = sizeOfResultedPolynomial;
    }

    public void sum(int[] firstPolynomial, int[] secondPolynomial, int sizeOfFirstPolynomial, int sizeOfSecondPolynomial){
        if(sizeOfFirstPolynomial >= sizeOfSecondPolynomial){        // we check which polynomial is greater
            resultedPolynomial = new int[sizeOfFirstPolynomial];    // in order to set the resulted polynomial size as the greater one
            sizeOfResultedPolynomial = sizeOfFirstPolynomial;
        }
        else{
            resultedPolynomial = new int[sizeOfSecondPolynomial];
            sizeOfResultedPolynomial = sizeOfSecondPolynomial;
        }
        if( sizeOfFirstPolynomial == sizeOfSecondPolynomial) {
            for (int i = 0; i < sizeOfResultedPolynomial; i++) {
                resultedPolynomial[i] = firstPolynomial[i] + secondPolynomial[i];  // addition of the coefficients
            }
        }
        else{
            if( sizeOfSecondPolynomial < sizeOfFirstPolynomial ){
                int i;
                for(i = 0 ; i < sizeOfSecondPolynomial ; i++){
                    resultedPolynomial[i] = firstPolynomial[i] + secondPolynomial[i]; // addition of the coefficients
                }
                for(int j = i ; j < sizeOfFirstPolynomial ; j++){
                    resultedPolynomial[j] = firstPolynomial[j];         // filling the rest of the terms unchanged because they do not have a term to be added to
                }
            }
            else{
                if( sizeOfFirstPolynomial < sizeOfSecondPolynomial ){
                    int i;
                    for( i = 0 ; i < sizeOfFirstPolynomial ; i++){
                        resultedPolynomial[i] = firstPolynomial[i] + secondPolynomial[i];   // addition of the coefficients
                    }
                    for(int j = i ; j < sizeOfSecondPolynomial ; j++){
                        resultedPolynomial[j] = secondPolynomial[j];    // filling the rest of the terms unchanged because they do not have a term to be added to
                    }
                }
            }
        }
        String[] auxResult = new String[sizeOfResultedPolynomial];      // auxResult is used in order to start concatenating the resulted monomials in the final polynomial
        for(int i = 0; i < sizeOfResultedPolynomial; i++){
            if(resultedPolynomial[i] >= 0){
                auxResult[i] = "+";
                auxResult[i] = auxResult[i].concat(String.valueOf(resultedPolynomial[i]));
            }
            else
                auxResult[i] = String.valueOf(resultedPolynomial[i]);
        }
        for(int i = sizeOfResultedPolynomial -1; i >= 0 ; i--){
            String power = String.valueOf(i);
            finalResult = finalResult.concat(auxResult[i]);     // final polynomial is being constructed
            finalResult = finalResult.concat("x^");
            finalResult = finalResult.concat(power);
        }
        Interface accessInterface = new Interface();
        JOptionPane.showMessageDialog(accessInterface.frame, finalResult);  // resulted polynomial display
    }

    public void difference(int[] firstPolynomial, int[] secondPolynomial, int sizeOfFirstPolynomial, int sizeOfSecondPolynomial){
        if(sizeOfFirstPolynomial >= sizeOfSecondPolynomial){
            resultedPolynomial = new int[sizeOfFirstPolynomial];
            sizeOfResultedPolynomial = sizeOfFirstPolynomial;
        }
        else{
            resultedPolynomial = new int[sizeOfSecondPolynomial];
            sizeOfResultedPolynomial = sizeOfSecondPolynomial;
        }
        if( sizeOfFirstPolynomial == sizeOfSecondPolynomial) {
            for (int i = 0; i < sizeOfResultedPolynomial; i++) {
                resultedPolynomial[i] = firstPolynomial[i] - secondPolynomial[i];
            }
        }
        else{
            if( sizeOfSecondPolynomial < sizeOfFirstPolynomial ){
                int i;
                for(i = 0 ; i < sizeOfSecondPolynomial ; i++){
                    resultedPolynomial[i] = firstPolynomial[i] - secondPolynomial[i];
                }
                for(int j = i ; j < sizeOfFirstPolynomial ; j++){
                    resultedPolynomial[j] = firstPolynomial[j];
                }
            }
            else{
                if( sizeOfFirstPolynomial < sizeOfSecondPolynomial ){
                    int i;
                    for( i = 0 ; i < sizeOfFirstPolynomial ; i++){
                        resultedPolynomial[i]= firstPolynomial[i] - secondPolynomial[i];
                    }
                    for(int j = i ; j < sizeOfSecondPolynomial ; j++){
                        resultedPolynomial[j] = secondPolynomial[j];
                    }
                }
            }
        }
        String[] auxResult = new String[sizeOfResultedPolynomial];
        for(int i = 0; i < sizeOfResultedPolynomial; i++){
            if(resultedPolynomial[i] >= 0){
                auxResult[i] = "+";
                auxResult[i] = auxResult[i].concat(String.valueOf(resultedPolynomial[i]));
            }
            else
                auxResult[i] = String.valueOf(resultedPolynomial[i]);
        }
        for(int i = sizeOfResultedPolynomial -1; i >= 0 ; i--){
            String power = String.valueOf(i);
            finalResult = finalResult.concat(auxResult[i]);
            finalResult = finalResult.concat("x^");
            finalResult = finalResult.concat(power);
        }
        Interface accessInterface = new Interface();
        JOptionPane.showMessageDialog(accessInterface.frame, finalResult);
    }

    public void multiply(int[] firstPolynomial, int[] secondPolynomial, int sizeOfFirstPolynomial, int sizeOfSecondPolynomial){
        sizeOfResultedPolynomial = sizeOfFirstPolynomial + sizeOfSecondPolynomial - 1;
        resultedPolynomial = new int[sizeOfResultedPolynomial];

        for(int i = 0 ; i < sizeOfFirstPolynomial ; i++){
            for(int j = 0 ; j < sizeOfSecondPolynomial ; j++){
                int k = i+j;
                resultedPolynomial[k] = resultedPolynomial[k] + (firstPolynomial[i] * secondPolynomial[j]);
            }
        }

        String[] auxResult = new String[sizeOfResultedPolynomial];

        for(int i = 0; i < sizeOfResultedPolynomial; i++){
            if(resultedPolynomial[i] >= 0){
                auxResult[i] = "+";
                auxResult[i] = auxResult[i].concat(String.valueOf(resultedPolynomial[i]));
            }
            else
                auxResult[i] = String.valueOf(resultedPolynomial[i]);
        }

        for(int i = sizeOfResultedPolynomial -1; i >= 0 ; i--){
            String power = String.valueOf(i);
            finalResult = finalResult.concat(auxResult[i]);
            finalResult = finalResult.concat("x^");
            finalResult = finalResult.concat(power);
        }
        Interface accessInterface = new Interface();
        JOptionPane.showMessageDialog(accessInterface.frame, finalResult);
    }


}
