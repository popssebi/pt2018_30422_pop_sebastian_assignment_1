package control;

public class PolynomialAndMonomial {

    private int[] firstPolynomial = new int[999];                // arrays are used to store polynomial data as it follows:
    private int[] secondPolynomial = new int[999];               // every coefficient is stored at its power, which is the array's index
    private int sizeOfFirstPolynomial, sizeOfSecondPolynomial;   // variables used for the size of the 2 arrays

    public int[] getFirstPolynomial() {
        return firstPolynomial;
    }

    public void setFirstPolynomial(int[] firstPolynomial) {
        this.firstPolynomial = firstPolynomial;
    }

    public int[] getSecondPolynomial() {
        return secondPolynomial;
    }

    public void setSecondPolynomial(int[] secondPolynomial) {
        this.secondPolynomial = secondPolynomial;
    }

    public int getSizeOfFirstPolynomial() {
        return sizeOfFirstPolynomial;
    }

    public void setSizeOfFirstPolynomial(int sizeOfFirstPolynomial) {
        this.sizeOfFirstPolynomial = sizeOfFirstPolynomial;
    }

    public int getSizeOfSecondPolynomial() {
        return sizeOfSecondPolynomial;
    }

    public void setSizeOfSecondPolynomial(int sizeOfSecondPolynomial) {
        this.sizeOfSecondPolynomial = sizeOfSecondPolynomial;
    }

    public void getFirstPolynomial(String auxString){

        char firstChar;
        firstChar = auxString.charAt(0);           // firstChar is used to store the first char of the polynomial added by the user

        String replacedAuxString = auxString.replace("-","+-"); // in the given polynomial we replace '-' with '+-' in order to correctly split the polynomial into monomials
        String[] splitString = replacedAuxString.split("[+]");            // we split the polynomial at the '+' character
        sizeOfFirstPolynomial = splitString.length;

        if(firstChar == '-'){                      // we check if the first monomial of the polynomial is a negative one, if it is, we need to shift left in order to recover the correct sign of the monomial
                                                   // example: -2x^3+5x^2-12x^1+6x^0 => replaceAusString => +-2x^3+5x^2+-12x^1+6x^0 => splitString => [+-2x^3, 5x^2, -12x^1, 6x^0]
            for(int j = 0; j < sizeOfFirstPolynomial -1 ; j++){        // so we need to fix the first monomial by having -2x^3 instead of +-2x^3
                splitString[j]=splitString[j+1];
            }

            sizeOfFirstPolynomial--;
        }

        for(int z = 0; z < sizeOfFirstPolynomial; z++ ){

            String[] aux = splitString[z].split("[*x^]"); // after having the monomials we split every monomial in order to remain with only the coefficient and the power
            int coefficient = Integer.valueOf(aux[0]);         // we convert the coefficient from String to Integer
            int power = sizeOfFirstPolynomial -z-1;            // to get the power we take the size of the polynomial and subtract the current index and 1 out of it
            firstPolynomial[power]=coefficient;                // we store the coefficient in array of index 'power' => array[power]=coefficient
        }

    }

    public void getSecondPolynomial(String auxString) {

        char fisrtChar;                     // firstChar is used to store the first char of the polynomial added by the user
        fisrtChar = auxString.charAt(0);

        String replacedAuxString = auxString.replace("-", "+-");  // in the given polynomial we replace '-' with '+-' in order to correctly split the polynomial into monomials
        String[] splitString = replacedAuxString.split("[+]");              // we split the polynomial at the '+' character
        sizeOfSecondPolynomial = splitString.length;

        if (fisrtChar == '-') {             // we check if the first monomial of the polynomial is a negative one, if it is, we need to shift left in order to recover the correct sign of the monomial
                                            // example: -2x^3+5x^2-12x^1+6x^0 => replaceAusString => +-2x^3+5x^2+-12x^1+6x^0 => splitString => [+-2x^3, 5x^2, -12x^1, 6x^0]
            for (int j = 0; j < sizeOfSecondPolynomial - 1; j++) {          // so we need to fix the first monomial by having -2x^3 instead of +-2x^3
                splitString[j] = splitString[j + 1];
            }

            sizeOfSecondPolynomial--;
        }

        for (int z = 0; z < sizeOfSecondPolynomial; z++) {

            String[] aux = splitString[z].split("[*x^]");   // after having the monomials we split every monomial in order to remain with only the coefficient and the power
            int coefficient = Integer.valueOf(aux[0]);           // we convert the coefficient from String to Integer
            int power = sizeOfSecondPolynomial - z - 1;          // to get the power we take the size of the polynomial and subtract the current index and 1 out of it
            secondPolynomial[power] = coefficient;               // we store the coefficient in array of index 'power' => array[power]=coefficient
        }

    }

    public void getThirdPolynomial(int pickOperation){      // the operation method on polynomials is called depending on the operation picked by the user

        Operations accessOperations = new Operations();

        switch (pickOperation) {
            case 1: accessOperations.sum(firstPolynomial, secondPolynomial, sizeOfFirstPolynomial, sizeOfSecondPolynomial);
                break;
            case 2: accessOperations.difference(firstPolynomial, secondPolynomial, sizeOfFirstPolynomial, sizeOfSecondPolynomial);
                break;
            case 3: accessOperations.multiply(firstPolynomial, secondPolynomial, sizeOfFirstPolynomial, sizeOfSecondPolynomial);
        }

    }
}
