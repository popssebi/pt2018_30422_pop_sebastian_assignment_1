package view;
import control.PolynomialAndMonomial;

import javax.swing.*;

public class Interface {

    private int pickOperation;                           // variable used to choose the operation which will be applied on the 2 polynomials: 1=sum, 2=difference and 3=multiply
    private String firstPolynomial="";                   // string used to store the data for the 1st polynomial added by the user in "firstPolText"
    private String secondPolynomial ="";                 // string used to store the data for the 2nd polynomial added by the user in "secondPolText"
    public  JFrame frame = new JFrame();
    private JPanel panel = new JPanel(null,true);
    private JLabel firstPolLabel = new JLabel("1st polynomial:");
    private JTextField firstPolText = new JTextField();                     // TextField used as the place where the user introduces 1st polynomial data
    private JLabel secondPolLabel = new JLabel("2nd polynomial:");
    private JTextField secondPolText = new JTextField();                    // TextField used as the place where the user introduces 2nd polynomial data
    private JButton sumButton = new JButton("Sum");                     // sumButton is the button which, if pressed by the user, will result the sum of the 2 added polynomials
    private JButton differenceButton = new JButton("Difference");       // differenceButton is the button which, if pressed by the user, will result the difference between the 2 polynomials
    private JButton multiplyButton = new JButton("Multiply");           // multiplyButton is the button which, if pressed by the user, will result the multiplication of the 2 added polynomials
    private JLabel examplePolLabel = new JLabel("Input example: -16x^3+8x^2-7x+9");     // examplePolLabel is a label which will give the user an input example of how to correctly introduce input data

    public int getPickOperation() {
        return pickOperation;
    }

    public void setPickOperation(int pickOperation) {
        this.pickOperation = pickOperation;
    }

    public String getFirstPolynomial() {
        return firstPolynomial;
    }

    public void setFirstPolynomial(String poli1) {
        this.firstPolynomial = poli1;
    }

    public String getSecondPolynomial() {
        return secondPolynomial;
    }

    public void setSecondPolynomial(String secondPolynomial) {
        this.secondPolynomial = secondPolynomial;
    }

    public JFrame getFrame() {
        return frame;
    }

    public void setFrame(JFrame frame) {
        this.frame = frame;
    }

    public JPanel getPanel() {
        return panel;
    }

    public void setPanel(JPanel panel) {
        this.panel = panel;
    }

    public JLabel getFirstPolLabel() {
        return firstPolLabel;
    }

    public void setFirstPolLabel(JLabel firstPolLabel) {
        this.firstPolLabel = firstPolLabel;
    }

    public JTextField getFirstPolText() {
        return firstPolText;
    }

    public void setFirstPolText(JTextField firstPolText) {
        this.firstPolText = firstPolText;
    }

    public JLabel getSecondPolLabel() {
        return secondPolLabel;
    }

    public void setSecondPolLabel(JLabel secondPolLabel) {
        this.secondPolLabel = secondPolLabel;
    }

    public JTextField getSecondPolText() {
        return secondPolText;
    }

    public void setSecondPolText(JTextField secondPolText) {
        this.secondPolText = secondPolText;
    }

    public JButton getSumButton() {
        return sumButton;
    }

    public void setSumButton(JButton sumButton) {
        this.sumButton = sumButton;
    }

    public JButton getDifferenceButton() {
        return differenceButton;
    }

    public void setDifferenceButton(JButton differenceButton) {
        this.differenceButton = differenceButton;
    }

    public JButton getMultiplyButton() {
        return multiplyButton;
    }

    public void setMultiplyButton(JButton multiplyButton) {
        this.multiplyButton = multiplyButton;
    }

    public void layout(){

        //================================= FRAME ========================================

        frame.setLayout(null);
        frame.setSize(500, 400);                    // set the size of the program's frame
        frame.setTitle("Polynomial Processing");              // set program's (frame's) title
        frame.setDefaultCloseOperation(JFrame.EXIT_ON_CLOSE); // because of this function, when the user presses the 'X' button in the right-up corner the whole program will be closed, not only the frame
                                                              // without this function the program would still remain open in the memory
        panel.setLayout(null);
        panel.setSize(500,400);       // set the size of the panel
        panel.setLocation(0,0);              // being located at (0,0) and having the same size => panel overlay's the frame

        //========================= 1st & 2nd Polynomials ================================

        firstPolLabel.setSize(90,20);
        firstPolLabel.setLocation(150,20);
        panel.add(firstPolLabel);

        firstPolText.setSize(200,20);
        firstPolText.setLocation(150,45);
        panel.add(firstPolText);

        secondPolLabel.setSize(90,20);
        secondPolLabel.setLocation(150,85);
        panel.add(secondPolLabel);

        secondPolText.setSize(200,20);
        secondPolText.setLocation(150,110);
        panel.add(secondPolText);

        //=========================== Operation Buttons ================================

        sumButton.setSize(100,30);
        sumButton.setLocation(200,155);
        panel.add(sumButton);

        differenceButton.setSize(100,30);
        differenceButton.setLocation(200,205);
        panel.add(differenceButton);

        multiplyButton.setSize(100,30);
        multiplyButton.setLocation(200,255);
        panel.add(multiplyButton);

        //============================= Example ==================================

        examplePolLabel.setSize(200,20);
        examplePolLabel.setLocation(150,295);
        panel.add(examplePolLabel);

        frame.add(panel);                // add the panel with all the components to the frame
        frame.setVisible(true);          // make the frame visible for the user

        //============================== Actions ==================================

        sumButton.addActionListener(e -> {      // if the sumButton is pressed by the user:
            pickOperation = 1;                  // pickOperation=1 which will later be used to know with operation method to call

            firstPolynomial = firstPolText.getText();       // read the 2 polynomials added by the user
            secondPolynomial = secondPolText.getText();

            PolynomialAndMonomial accessPolynomialAndMonomial = new PolynomialAndMonomial();
            accessPolynomialAndMonomial.getFirstPolynomial(firstPolynomial);
            accessPolynomialAndMonomial.getSecondPolynomial(secondPolynomial);      // send the 2 polynomials in ordered be processed in the PolynomialAndMonomial class
            accessPolynomialAndMonomial.getThirdPolynomial(pickOperation);          // along with the operation picked by the user
        });

        differenceButton.addActionListener(e -> {      // if the differenceButton is pressed by the user:
            pickOperation = 2;                         // pickOperation=2 which will later be used to know with operation method to call

            firstPolynomial = firstPolText.getText();  // read the 2 polynomials added by the user
            secondPolynomial = secondPolText.getText();

            PolynomialAndMonomial accessPolynomialAndMonomial = new PolynomialAndMonomial();
            accessPolynomialAndMonomial.getFirstPolynomial(firstPolynomial);
            accessPolynomialAndMonomial.getSecondPolynomial(secondPolynomial);      // send the 2 polynomials in ordered be processed in the PolynomialAndMonomial class
            accessPolynomialAndMonomial.getThirdPolynomial(pickOperation);          // along with the operation picked by the user
        });

        multiplyButton.addActionListener(e -> {         // if the multiplyButton is pressed by the user:
            pickOperation = 3;                          // pickOperation=3 which will later be used to know with operation method to call

            firstPolynomial = firstPolText.getText();   // read the 2 polynomials added by the user
            secondPolynomial = secondPolText.getText();

            PolynomialAndMonomial accessPolynomialAndMonomial = new PolynomialAndMonomial();
            accessPolynomialAndMonomial.getFirstPolynomial(firstPolynomial);
            accessPolynomialAndMonomial.getSecondPolynomial(secondPolynomial);      // send the 2 polynomials in ordered be processed in the PolynomialAndMonomial class
            accessPolynomialAndMonomial.getThirdPolynomial(pickOperation);          // along with the operation picked by the user
        } );

    }
}
